import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

enum Gender {
  Male,
  Female,
  Other,
}

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  late String name;
  late String phoneNumber;
  Gender selectedGender = Gender.Other;
  DateTime? selectedDate;
  bool showError = false;

  Future<void> _showDatePicker(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(1900),
      lastDate: DateTime.now(),
    );
    if (picked != null) {
      setState(() {
        selectedDate = picked;
      });
    }
  }

  void _showConfirmationDialog() {
    if (name.isNotEmpty && phoneNumber.isNotEmpty && selectedDate != null) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Confirmación'),
            content: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  'Por favor, confirma los datos ingresados:',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 16.0),
                Text('Nombre: $name'),
                Text('Teléfono: $phoneNumber'),
                Text('Género: ${_getGenderText(selectedGender)}'),
                Text('Fecha de Nacimiento: ${_getFormattedBirthDate()}'),
              ],
            ),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text('Cancelar'),
              ),
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ProfilePage(
                        name: name,
                        phoneNumber: phoneNumber,
                        gender: selectedGender,
                        birthDate: selectedDate,
                      ),
                    ),
                  );
                },
                child: Text('Aceptar'),
              ),
            ],
          );
        },
      );
    } else {
      setState(() {
        showError = true;
      });
    }
  }

  String _getGenderText(Gender gender) {
    switch (gender) {
      case Gender.Male:
        return 'Masculino';
      case Gender.Female:
        return 'Femenino';
      case Gender.Other:
      default:
        return 'Otro';
    }
  }

  String _getFormattedBirthDate() {
    return selectedDate != null
        ? selectedDate!.toString().split(' ')[0]
        : 'N/A';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Inicio de sesión'),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              'Ingresa tus datos:',
              style: TextStyle(
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 16.0),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.0),
                border: Border.all(
                  color: Colors.grey,
                  width: 1.0,
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      'Nombre',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  TextField(
                    onChanged: (value) {
                      setState(() {
                        name = value;
                      });
                    },
                  ),
                ],
              ),
            ),
            SizedBox(height: 16.0),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.0),
                border: Border.all(
                  color: Colors.grey,
                  width: 1.0,
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      'Teléfono',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  TextField(
                    onChanged: (value) {
                      setState(() {
                        phoneNumber = value;
                      });
                    },
                    keyboardType: TextInputType.phone,
                    inputFormatters: [
                      FilteringTextInputFormatter.digitsOnly,
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(height: 16.0),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.0),
                border: Border.all(
                  color: Colors.grey,
                  width: 1.0,
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      'Género',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  ListTile(
                    title: Text('Masculino'),
                    leading: Radio<Gender>(
                      value: Gender.Male,
                      groupValue: selectedGender,
                      onChanged: (Gender? value) {
                        setState(() {
                          selectedGender = value!;
                        });
                      },
                    ),
                  ),
                  ListTile(
                    title: Text('Femenino'),
                    leading: Radio<Gender>(
                      value: Gender.Female,
                      groupValue: selectedGender,
                      onChanged: (Gender? value) {
                        setState(() {
                          selectedGender = value!;
                        });
                      },
                    ),
                  ),
                  ListTile(
                    title: Text('Otro'),
                    leading: Radio<Gender>(
                      value: Gender.Other,
                      groupValue: selectedGender,
                      onChanged: (Gender? value) {
                        setState(() {
                          selectedGender = value!;
                        });
                      },
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 16.0),
            TextButton(
              onPressed: () => _showDatePicker(context),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.calendar_today),
                  SizedBox(width: 8.0),
                  Text(
                    'Seleccionar Fecha de Nacimiento',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            ),
            if (showError)
              Padding(
                padding: EdgeInsets.only(top: 16.0),
                child: Text(
                  'Por favor, completa todos los campos',
                  style: TextStyle(
                    color: Colors.red,
                  ),
                ),
              ),
            SizedBox(height: 16.0),
            Center(
              child: ElevatedButton(
                onPressed: _showConfirmationDialog,
                child: Text('Iniciar sesión'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}class ProfilePage extends StatelessWidget {
  final String name;
  final String phoneNumber;
  final Gender gender;
  final DateTime? birthDate;

  ProfilePage({
    required this.name,
    required this.phoneNumber,
    required this.gender,
    required this.birthDate,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Perfil'),
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 16.0),
            Center(
              child: CircleAvatar(
                radius: 60,
                backgroundImage: AssetImage('assets/imagenes/persona.jpg'),
              ),
            ),
            SizedBox(height: 16.0),
            Text(
              'Nombre: $name',
              style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 8.0),
            Text(
              'Teléfono: $phoneNumber',
              style: TextStyle(fontSize: 18.0),
            ),
            SizedBox(height: 8.0),
            Text(
              'Género: ${_getGenderText(gender)}',
              style: TextStyle(fontSize: 18.0),
            ),
            SizedBox(height: 8.0),
            Text(
              'Fecha de Nacimiento: ${birthDate != null ? birthDate!.toString().split(' ')[0] : 'N/A'}',
              style: TextStyle(fontSize: 18.0),
            ),
          ],
        ),
      ),
    );
  }

  String _getGenderText(Gender gender) {
    switch (gender) {
      case Gender.Male:
        return 'Masculino';
      case Gender.Female:
        return 'Femenino';
      case Gender.Other:
      default:
        return 'Otro';
    }
  }
}