import 'package:flutter/material.dart';
import 'login_page.dart';

class ProfilePage extends StatelessWidget {
  final String name;
  final String phoneNumber;
  final Gender gender;
  final DateTime? birthDate;

  ProfilePage({
    required this.name,
    required this.phoneNumber,
    required this.gender,
    required this.birthDate,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Perfil'),
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 16.0),
            Center(
              child: CircleAvatar(
                radius: 60,
                backgroundImage: AssetImage('assets/imagenes/persona.jpg'),
              ),
            ),
            SizedBox(height: 16.0),
            Text(
              'Nombre: $name',
              style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 8.0),
            Text(
              'Teléfono: $phoneNumber',
              style: TextStyle(fontSize: 18.0),
            ),
            SizedBox(height: 8.0),
            Text(
              'Género: ${gender == Gender.Male ? 'Masculino' : gender == Gender.Female ? 'Femenino' : 'Otro'}',
              style: TextStyle(fontSize: 18.0),
            ),
            SizedBox(height: 8.0),
            Text(
              'Fecha de Nacimiento: ${birthDate != null ? birthDate!.toString().split(' ')[0] : 'N/A'}',
              style: TextStyle(fontSize: 18.0),
            ),
          ],
        ),
      ),
    );
  }
}
